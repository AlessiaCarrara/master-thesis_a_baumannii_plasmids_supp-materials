#!usr/bin/bash


#DEFINE VARIABLES
genus=$1
specie=$2
specie_genomes=$3


#WGSIM: SIMULATE SHORT READS
#list_gz=$(ls *.gz)     #put the .fq.gz genome file in a variable
#for i in $list_gz ; do gunzip $i; done #gzip all the genomes
#ls *fna > genomes_names_$genus_$specie.txt #store the genomes in a .txt file
#awk '{print "wgsim" , $i, $i"_f1.fq", $i"_f2.fq", "-e 0", "-h"}' genomes_names_$genus_$specie.txt > wgsim_commands.sh #print for each gen$
#bash wgsim_commands.sh > wgsim

#echo Short reads simulated


#SPADES: DE-NOVO ASSEMBLY
#ls  *.fna > fna_genomes.txt		#create a list of all genomes (.fna)
#awk '{print "spades.py" , "-1", $1"_f1.fq", "-2", $1"_f2.fq", "-o", $1"_denovo_assembly", "--only-assembler"}' fna_genomes.txt > commands_spades.sh #create the command for all

#echo Spades is running

#bash commands_spades.sh > spades #run and create the contig.fasta in each folder for each isolate

#echo De-novo assembly completed

#MOVE EACH GENOME IN EACH GENOME FOLDER

for i in $(cat fna_genomes.txt)
do
cp $i "$i"_denovo_assembly/ #copy the .fna genome in the folder called with its name
done

echo Each genome moved in the specific genome folder

awk '{print $i"_denovo_assembly"}' fna_genomes.txt > folder_names.txt #create a file with the list of the folder names of each genomes


#BWA: MAPPING AGAINST REFERENCE GENOMES

echo Bwa is running...

list_folders_names=$(cat folder_names.txt)
for i in $list_folders_names
do
cd $i
bwa index *.fna
bwa mem -x ont2d *.fna contigs.fasta > contigs_map.sam
samtools view -bS -F 4 contigs_map.sam > contigs_map.bam
samtools sort contigs_map.bam -o contigs_map_sorted.bam
samtools index contigs_map_sorted.bam
samtools view contigs_map_sorted.bam | awk -v name=$i '{print $1,"\t",$3,"\t", name}'> first_view_bam.txt #file with contig name, sequence, genome folder name
cat first_view_bam.txt >> ../mix_first_view_bam.txt

cd ..			
done

echo bam file produced for each genome and all bam files collected in a single one "mix_first_view_bam.txt"

cat *.fna > complete_genomes.fna

echo Complete genome sequences stored in "complete_genomes.fna" file


for i in $list_folders_name
do
cd $i
cat contigs.fasta >> ../contigs_total.fasta
cd ..
done

echo Total contigs of all isolates stored in "contigs_total.fasta" file

